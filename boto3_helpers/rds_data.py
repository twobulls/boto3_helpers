Conversions = {
    'isNull': lambda x: None,
    'realValue': lambda x: str(x),
    'doubleValue': lambda x: str(x)
}


def parse_response(response: dict) -> dict:
    """
    Given a response from rds-data ``https://docs.aws.amazon.com/cli/latest/reference/rds-data/execute-sql.html``
    convert to a list of results with key value pairs
    """

    def parse_col_type(val_json):
        """
        Run a conversion lambda (if available) on a given column value
        """
        key, val = list(val_json.items())[0]
        if key in Conversions:
            return Conversions[key](val)
        return val
           
    results = []

    if 'sqlStatementResults' in response:
        for sql_results in response['sqlStatementResults']:
            statement_rows = []
            if sql_results['numberOfRecordsUpdated'] < 0:
                frame = sql_results['resultFrame']
                for row_json in frame['records']:
                    row = {}
                    for col_num in range(0, frame['resultSetMetadata']['columnCount']):
                        meta = frame['resultSetMetadata']['columnMetadata'][col_num]
                        val_json = row_json['values'][col_num]
                        row[meta['label']] = parse_col_type(val_json)

                    statement_rows.append(row)
            results.append(statement_rows)

    elif 'records' in response:
        if 'columnMetadata' not in response:
            raise Exception("Use 'includeResultMetadata' in execute_statement call so results include metadata")
        
        for sql_result in response['records']:
            row = {}
            if len(sql_result) == 0:
                continue
            for col_num in range(0, len(response['columnMetadata'])):
                meta = response['columnMetadata'][col_num]
                val_json = sql_result[col_num]
                row[meta['label']] = parse_col_type(val_json)
            results.append(row)
    elif 'numberOfRecordsUpdated' in response:
        pass
    else:
        raise Exception("Unknown response. Cannot parse.")        
    
    return results

def parse_parameters(params: dict):
    """
    Given a dictionary of parameters, converts to
    parameters expected by execute_statement
    """

    TYPES = {
      'str': 'stringValue',
      'int': 'longValue',
      'float': 'doubleValue',
      'NoneType': 'isNull',
      'bool': 'booleanValue'
    }

    return [
      {
        'name': key,
        'value': {
          TYPES[type(value).__name__]: value if value is not None else True
        }
      } for key, value in params.items()
    ]
