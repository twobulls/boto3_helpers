#!/usr/bin/env bash
set -e

virtualenv --python=python3 venv
source venv/bin/activate
pip install pytest
pytest
deactivate
