import pytest
import os
import json

import sys
myPath = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, myPath + '/../')


from boto3_helpers.rds_data import parse_parameters

def test_dataapi_parameter_parser_success():

    test_dict = {
        'aString': 'hello',
        'aInt': 123,
        'aBool': True,
        'aNull': None,
        'aFloat': 1.234
    }

    resp = parse_parameters(test_dict)
    print(resp)

    def get_param(p):
        res = next(iter([r for r in resp if r['name'] == p]), None)
        if res is None:
            assert 1/0
        return res['value']
    
    assert get_param('aString') == { 'stringValue': 'hello' }
    assert get_param('aInt') == { 'longValue': 123 }
    assert get_param('aBool') == { 'booleanValue': True }
    assert get_param('aNull') == { 'isNull': True }
    assert get_param('aFloat') == { 'doubleValue': 1.234 }
        

