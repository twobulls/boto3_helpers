import pytest
import os
import json

import sys
myPath = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, myPath + '/../')


from boto3_helpers.rds_data import parse_response

def json_file(filename):
    def decorator_func(func):
        def inner(**kwargs):
            with open(filename, 'r') as f:
                j = json.loads(f.read())
                res = func(j,**kwargs)
            return res
        return inner
    return decorator_func

@json_file('tests/response_single_statement.json')
def test_dataapi_reponse_parsers_single_statement(response):
    parsed_statements = parse_response(response)
    assert len(parsed_statements) == 1

    statement_results = parsed_statements[0]
    assert len(statement_results) == 10

    row_1 = statement_results[0]

    assert row_1['id'] == '01a0e8ab-b379-41a9-a547-5dcb393f172a'
    assert row_1['accountId'] == '3f692d1b-7854-4429-9805-de03e2b05e24'
    assert row_1['name'] == 'Test 1'
    assert row_1['createdAt'] == '2019-03-18 00:06:06.641'
    assert row_1['dob'] is None

@json_file('tests/response_multiple_statements.json')
def test_dataapi_reponse_parsers_multiple_statement(response):
    parsed_statements = parse_response(response)

    # confirm multiple statements returned
    assert len(parsed_statements) == 2

    statement_results_1 = parsed_statements[0]
    assert len(statement_results_1) == 3

    statement_results_2 = parsed_statements[1]
    assert len(statement_results_2) == 5

    # assert that decimals are converted to strings
    row_1 = statement_results_1[0]
    assert row_1['locationLat'] == '38.099998474121094'

@json_file('tests/response_insert_statement.json')
def test_dataapi_reponse_parsers_handles_mutation(response):
    parsed_statements = parse_response(response)
    assert len(parsed_statements) == 1
    statement_results_1 = parsed_statements[0]
    assert len(statement_results_1) == 0

@json_file('tests/response_v2_single_statement.json')
def test_dataapi_response_v2_parses(response):
    parsed_statement = parse_response(response)
    assert len(parsed_statement) == 2
    
    row_1 = parsed_statement[0]
    assert row_1['productID'] == 1
    assert row_1['productCode'] == 'ABC'
    assert row_1['name'] == 'Test1'
    assert row_1['quantity'] is None
    assert row_1['price'] == "1.1"

@json_file('tests/response_v2_update_statement.json')
def test_dataapi_response_v2_parses(response):
    parsed_statement = parse_response(response)
    assert len(parsed_statement) == 0

@json_file('tests/response_v2_single_statement.json')
def test_dataapi_response_v2_parses_empty(response):
    response['records'] = []
    parsed_statement = parse_response(response)
    assert len(parsed_statement) == 0
