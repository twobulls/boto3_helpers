from setuptools import setup
from setuptools import find_packages

VERSION = '0.0.4'

setup(
    name='boto3_helpers',
    version=VERSION,
    description='This library contains helper functions useful when developing against the popular boto3 library',
    long_description='',
    url='https://bitbucket.org/twobulls/boto3_helpers/',
    author='Two Bulls',
    license='Apache License 2.0',
    tests_require=['pytest >= 2.5.2'],
    packages=find_packages(exclude=['tests*']),
    include_package_data=True,
    classifiers=[
        'Programming Language :: Python'
    ]
)
