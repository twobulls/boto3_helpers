# Overview

This library contains helper functions useful when developing against the popular boto3 library

# Requirements

* Python (3.6, 3.7)

# Installation

* `pip install -e git+https://bitbucket.org/twobulls/boto3_helpers#egg=boto3_helpers`

# building new dist

Update version in setup.cfg

Run:
`python sdist bdist_wheel`

## rds_data module

Functions

### parse_response(response:dict) -> dict

This function parses the response from rds_data execute_statement function

It will also parse the old DEPRECATED execute_sql function.

```
import boto3
from boto3_helper import rds_data

client = boto3.client('rds-data')

response = client.execute_statement(
    sql='select * from atable',
    includeResultMetadata=True,
    **CREDS
)
parsed = rds_data.parse_response(response)


[**DEPRECATED**]
response = client.execute_sql(...)
parsed = rds_data.parse_response(response)


```

This will return data in the following format

```
[[
    {
        "id": 123,
        "name": "steve"
    },
    {
        "id": 456,
        "name": "ben"
    }
]]
```


